var gulp = require('gulp');
var browserSync = require('browser-sync').create();
var sass = require('gulp-sass');
var autoprefixer = require('gulp-autoprefixer');
var cleanCSS = require('gulp-clean-css');

gulp.task('serve', ['sass'], function() {

  browserSync.init({
    server: {
        baseDir: "./"
    }
  });

  gulp.watch('scss/*.scss', ['sass']);
  gulp.watch('**/*.html').on('change', browserSync.reload);
});

//move css and html to build folder after processing
gulp.task('build', ['minify-css'], function() {
  return gulp.src("scss/*.scss", ['minify-css']);
  return gulp.src("*.html")
    .pipe(gulp.dest("build"))
});

//compile scss to css, autoprefix and then minify
gulp.task('minify-css', function() {
  return gulp.src('*.css')
    .pipe(sass())
    .pipe(autoprefixer({
      browsers: ['last 2 versions'],
      cascade: false
    }))
    .pipe(cleanCSS({compatibility: 'ie8'}))
    .pipe(gulp.dest('build'));
});

// Compile sass into CSS, auto-prefix & auto-inject into browsers
gulp.task('sass', function() {
    return gulp.src("scss/*.scss")
        .pipe(sass())
        .pipe(autoprefixer({
          browsers: ['last 2 versions'],
          cascade: false
        }))
        .pipe(gulp.dest("./"))
        .pipe(browserSync.stream());
});

gulp.task('default', ['serve']);
