'use strict';
var yeoman = require('yeoman-generator');
var chalk = require('chalk');
var yosay = require('yosay');

module.exports = yeoman.Base.extend({
  prompting: function () {
    var done = this.async();

    // Have Yeoman greet the user.
    this.log(yosay(
      'Welcome to the stunning ' + chalk.red('tyler') + ' generator!'
    ));

    var prompts = [{
      type: 'input',
      name: 'name',
      message: 'What is the name of your project?',
      default: this.appname
    }, {
      type: 'input',
      name: 'description',
      message: 'What kind of project is this? (short description)'
    }];

    return this.prompt(prompts).then(function (answers) {
      // To access props later use this.props.someAnswer;
      this.props = answers;
      this.log(answers.name);
      done();
    }.bind(this));
  },

  writing: function () {
    var context = {
            name: this.props.name,
            description: this.props.description
        };
    this.template(this.templatePath('package.json'), this.destinationPath('package.json'), context);
    this.fs.copy(this.templatePath('_gulpfile.js'), this.destinationPath('gulpfile.js'));
    this.fs.copy(this.templatePath('scss/'), this.destinationPath('scss/'));
    this.fs.copy(this.templatePath('_index.html'), this.destinationPath('index.html'));
  },

  install: function () {
    this.installDependencies();
  }
});
