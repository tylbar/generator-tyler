# generator-tyler [![NPM version][npm-image]][npm-url] [![Build Status][travis-image]][travis-url] [![Dependency Status][daviddm-image]][daviddm-url]
> 

## Installation

First, install [Yeoman](http://yeoman.io) and generator-tyler using [npm](https://www.npmjs.com/) (we assume you have pre-installed [node.js](https://nodejs.org/)).

```bash
npm install -g yo
npm install -g generator-tyler
```

Then generate your new project:

```bash
yo tyler
```

## Getting To Know Yeoman

 * Yeoman has a heart of gold.
 * Yeoman is a person with feelings and opinions, but is very easy to work with.
 * Yeoman can be too opinionated at times but is easily convinced not to be.
 * Feel free to [learn more about Yeoman](http://yeoman.io/).

## License

MIT © [Tyler Barnes](http://tylerbarnes.ca)


[npm-image]: https://badge.fury.io/js/generator-tyler.svg
[npm-url]: https://npmjs.org/package/generator-tyler
[travis-image]: https://travis-ci.org//generator-tyler.svg?branch=master
[travis-url]: https://travis-ci.org//generator-tyler
[daviddm-image]: https://david-dm.org//generator-tyler.svg?theme=shields.io
[daviddm-url]: https://david-dm.org//generator-tyler
